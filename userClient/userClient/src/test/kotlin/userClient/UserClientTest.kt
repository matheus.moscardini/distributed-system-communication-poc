package userClient

import okhttp3.ConnectionPool
import userClient.config.HttpClientConfig
import java.time.Duration
import java.util.concurrent.TimeUnit
import kotlin.test.Test
import kotlin.test.assertEquals

class UserClientTest {

    lateinit var userClient: UserClient

    @Test
    fun defaultConfig() {
        userClient = UserClient(null)
        val response = userClient.getUser(1)
        assertEquals(response?.id, 1)
        assertEquals(response?.name, "Matheus")
    }

    @Test
    fun customDefaultConfig() {
        val config = HttpClientConfig.Builder().build()
        userClient = UserClient(config)
        val response = userClient.getUser(1)
        assertEquals(response?.id, 1)
        assertEquals(response?.name, "Matheus")
    }

    @Test
    fun customConfig() {
        val config = HttpClientConfig.Builder()
            .connectionPool(ConnectionPool(10, 10, TimeUnit.MINUTES))
            .readTimeout(Duration.ofMillis(5000))
            .writeTimeout(Duration.ofMillis(10000))
            .build()
        userClient = UserClient(config)
        val response = userClient.getUser(1)
        assertEquals(response?.id, 1)
        assertEquals(response?.name, "Matheus")
    }
}
