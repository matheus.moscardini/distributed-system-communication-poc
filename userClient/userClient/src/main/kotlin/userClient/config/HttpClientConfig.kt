package userClient.config


import okhttp3.Authenticator
import okhttp3.Cache
import okhttp3.ConnectionPool
import okhttp3.CookieJar
import okhttp3.Interceptor
import okhttp3.Protocol
import userClient.config.interceptors.DefaultContentTypeInterceptor
import java.time.Duration

open class HttpClientConfig private constructor(
    var authenticator : Authenticator?,
    var cache : Cache?,
    var connectionPool : ConnectionPool?,
    var connectTimeout : Duration,
    var readTimeout : Duration,
    var writeTimeout : Duration,
    var cookieJar : CookieJar?,
    var interceptors : List<Interceptor>?,
    var protocols : List<Protocol>?,
){
    class Builder {
        var authenticator : Authenticator? = null
        var cache : Cache? = null
        var connectionPool : ConnectionPool? = null
        var connectTimeout : Duration = Duration.ofMillis(5000)
        var readTimeout : Duration = Duration.ofMillis(5000)
        var writeTimeout : Duration = Duration.ofMillis(5000)
        var cookieJar : CookieJar? = null
        var interceptors : List<Interceptor>? = listOf(DefaultContentTypeInterceptor("application/json"))
        var protocols : List<Protocol>? = listOf(Protocol.HTTP_1_1)

        fun authenticator(authenticator: Authenticator) = apply { this.authenticator = authenticator }
        fun cache(cache: Cache) = apply { this.cache = cache }
        fun connectionPool(connectionPool: ConnectionPool) = apply { this.connectionPool = connectionPool }
        fun connectTimeout(connectTimeout: Duration) = apply { this.connectTimeout = connectTimeout }
        fun readTimeout(readTimeout: Duration) = apply { this.readTimeout = readTimeout }
        fun writeTimeout(writeTimeout: Duration) = apply { this.writeTimeout = writeTimeout }
        fun cookieJar(cookieJar: CookieJar) = apply { this.cookieJar = cookieJar }
        fun interceptors(interceptors: List<Interceptor>) = apply { this.interceptors = interceptors }

        fun build() = HttpClientConfig(authenticator, cache, connectionPool, connectTimeout, readTimeout, writeTimeout, cookieJar, interceptors, protocols)
    }
}