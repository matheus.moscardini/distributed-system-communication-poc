package userClient.config.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class DefaultContentTypeInterceptor : Interceptor {
    lateinit var contentType : String

    constructor(contentType: String) {
        this.contentType = contentType
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestWithUserAgent = originalRequest
            .newBuilder()
            .header("Content-Type", contentType)
            .build()
        return chain.proceed(requestWithUserAgent)
    }
}