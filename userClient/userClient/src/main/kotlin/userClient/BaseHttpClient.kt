package userClient

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import userClient.config.HttpClientConfig

open class BaseHttpClient(httpConfig : HttpClientConfig?)  {
    var client : OkHttpClient

    init {
        var config = httpConfig ?: HttpClientConfig.Builder().build()

        var clientBuilder : OkHttpClient.Builder = OkHttpClient.Builder();
        config.authenticator?.let { clientBuilder.authenticator(it) }
        config.cache?.let { clientBuilder.cache(it) }
        config.connectionPool?.let { clientBuilder.connectionPool(it) }
        config.connectTimeout?.let { clientBuilder.connectTimeout(it) }
        config.readTimeout?.let { clientBuilder.readTimeout(it) }
        config.writeTimeout?.let { clientBuilder.writeTimeout(it) }
        config.cookieJar?.let { clientBuilder.cookieJar(it) }
        config.interceptors?.let {
            for (interceptor : Interceptor in it) {
                clientBuilder.addInterceptor(interceptor)
            }
        }
        config.protocols?.let { clientBuilder.protocols(it) }

        client = clientBuilder.build()
    }
}