package userClient.DTO

import java.io.Serializable

open class UserDto : Serializable {

    var id : Long? = null
    var name : String? = null

    constructor()

}