package userClient

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.Call
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import userClient.DTO.UserDto
import userClient.config.HttpClientConfig

class UserClient : BaseHttpClient {
    constructor(config: HttpClientConfig?) : super(config)

    fun getUser(id : Long) : UserDto? {
        var request : Request = Request.Builder()
            .url("$baseUrl/$id")
            .build()

        var call : Call = client.newCall(request)
        var response : Response = call.execute()
        var user = decodeUserFromResponse(response.body!!)
        response.close()

        return user
    }

    private fun decodeUserFromResponse(response : ResponseBody) : UserDto? {
        return try {
            var mapper = jacksonObjectMapper()
            mapper.readValue(response.string(), UserDto::class.java)
        } catch (e : JsonProcessingException) {
            null
        } catch (e : JsonMappingException){
            null
        }
    }

    companion object {
        var baseUrl = "http://localhost:9000/user"
    }
}
