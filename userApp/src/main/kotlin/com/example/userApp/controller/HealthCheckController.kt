package com.example.userApp.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/health")
class HealthCheckController{
    val STATUS_MESSAGE = "Application is up"

    @GetMapping("/")
    fun getStatus() : String {
        println(STATUS_MESSAGE)
        return STATUS_MESSAGE
    }
}