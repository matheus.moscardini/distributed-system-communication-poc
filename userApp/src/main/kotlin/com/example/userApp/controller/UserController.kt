package com.example.userApp.controller

import com.example.userApp.model.User
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/user")
class UserController {

    @GetMapping("/{id}")
    fun getUser(@PathVariable id: Long) : User {
        System.out.println("Processing user with id $id")
        return User(1, "Matheus")
    }
}