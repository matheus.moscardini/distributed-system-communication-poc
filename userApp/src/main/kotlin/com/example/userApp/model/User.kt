package com.example.userApp.model

import java.io.Serializable

class User : Serializable {

    var id : Long? = null
    var name : String? = null

    constructor(id: Long?, name: String?) {
        this.id = id
        this.name = name
    }

}