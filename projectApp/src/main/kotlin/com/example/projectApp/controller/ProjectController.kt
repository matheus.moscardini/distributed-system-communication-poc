package com.example.projectApp.controller

import com.example.projectApp.model.Project
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import userClient.UserClient
@RestController
@RequestMapping("/project")
class ProjectController {
    val userClient : UserClient = UserClient(null)

    @GetMapping("/{id}")
    fun getProject(@PathVariable id: Long) : Project {

        println("Processing project with id $id")
        var user = userClient.getUser(id)
        return Project(1, "project test", user)
    }
}