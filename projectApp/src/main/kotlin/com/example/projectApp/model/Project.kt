package com.example.projectApp.model

import userClient.DTO.UserDto
import java.io.Serializable

class Project : Serializable {
    var id : Long? = null
    var name : String? = null
    var user : UserDto? = null

    constructor(id: Long?, name: String?, user: UserDto?) {
        this.id = id
        this.name = name
        this.user = user
    }
}