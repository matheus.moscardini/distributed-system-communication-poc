package com.example.projectApp.controller

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class ProjectControllerTest {

    @Autowired
    private lateinit var projectController: ProjectController

    @Autowired
    private lateinit var webClient : WebTestClient

    @Test
    fun getProject() {
        var response = webClient
            .get()
            .uri("/project/1")
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectBody()
                .jsonPath("$.name").isEqualTo("project test")
                .jsonPath("$.id").isEqualTo("1")
                .jsonPath("$.user.id").isEqualTo(1)
                .jsonPath("$.user.name").isEqualTo("Matheus")

        Assertions.assertEquals(1, 1)
    }
}