# distributed-system-communication-poc

Proof of concept for distributed systems' communication. 

## Requirements

- Install java 11
- Install HaProxy

## Running the project

### Building web client and server executables
## Installing the User Client Library
The userClient is a simple Gradle Library that exposes some methods to interact with the UserApp API. For this POC it must be installed locally. It must be installed inside the mvnRepo folder, since the projectApp configured this folder as a repository.

```
cd userClient
gradle build -x test
mvn install:install-file -Dfile={rootPath}/distributed-system-communication-poc/userClient/userClient/build/libs/userClient-0.1.0.jar -DgroupId=com.example -DartifactId=userClient -Dversion=0.1.0 -Dpackaging=jar -DlocalRepositoryPath={rootPath}/distributed-system-communication-poc/mvnRepo
```

Then build project app and user app with mvn package
```
cd projectApp
mvn clean install -DskipTests
mvn package -DskipTests 
cd ..
cd userApp
mvn clean install -DskipTests
mvn package -DskipTests
cd ..
```
### Running the project

**Running project App**
The project app is a simple API that is exposed on port 8081 and can receive HTTP requests.
```
java -jar projectApp/target/projectApp-0.0.1-SNAPSHOT.jar
```

**Running multiple instances of user App**
The haproxy.cnf looks for three web servers running on ports 9001, 9002 and 9003. On a production environment this would be substituted for different IP's that can use the same port, but for executing this POC locally we need to use different ports.
```
java -jar userApp/target/userApp-0.0.1-SNAPSHOT.jar --spring.application.json='{"server.port":9001}'
java -jar userApp/target/userApp-0.0.1-SNAPSHOT.jar --spring.application.json='{"server.port":9002}'
java -jar userApp/target/webServer-0.0.1-SNAPSHOT.jar --spring.application.json='{"server.port":9003}'
```
**Running HaProxy**
Finally we need to run HaProxy. It will act as our load balancer and distribute the load between our servers.

We need to copy all certificates to a shared file where HaProxy can find it.
```
sudo cp -r certificates/ /etc/haproxy/
```
Execute HaProxy pointing to the configuration in the root folder
```
haproxy -f haproxy.cfg
```
:warning: May be required to execute HaProxy with sudo

## Testing the application
To test this POC we can sent a simple HTTP request to the web Client application `/project/{id}` route. It will fetch some information from the server to build the final payload.
The client will look for port `9000` to connect to the web server. This port is handled by the load balancer.
```
curl --location --request GET 'http://localhost:8081/project/1'
```

We can then check that the server applications receive requests in a roundrobin order.
We can also check that if one of our servers is down HaProxy will remove this server from the rotation. Once this server is up again it will be added to the rotation automatically. 

### Debugging HaProxy

We can debug HaProxy by running the configuration with `-V` and `-d` arguments
```
haproxy -f haproxy.cfg -V -d
```
Also we are able to check our server status on `/haproxy_status` endpoint on port `6080`
```
http://localhost:6080/haproxy_stats
```